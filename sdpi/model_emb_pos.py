import numpy as np
import os
import pickle_save

from keras.models import Model, Input
from keras.layers import LSTM, Embedding, Dense, TimeDistributed, Dropout, Bidirectional,concatenate
from keras.layers import Bidirectional, concatenate, SpatialDropout1D, MaxPooling1D, Conv1D,Flatten
from keras.callbacks import EarlyStopping


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]= "1"
from keras import backend as K
K.tensorflow_backend._get_available_gpus()

embedding_matrix = pickle_save.read_pickle("embedding_matrix_pre") # we import the precalculated matrix
unique_words = pickle_save.read_pickle("unique_words_training_dev")
unique_pos = pickle_save.read_pickle("unique_POS_training_dev")
unique_tags = pickle_save.read_pickle("unique_tags_training_dev")

# import the training/dev/test data

x_train = pickle_save.read_pickle("X_train_pre")
p_train = pickle_save.read_pickle("P_train_pre")
y_train = pickle_save.read_pickle("y_train_pre")
x_dev = pickle_save.read_pickle("X_dev_pre")
p_dev = pickle_save.read_pickle("P_dev_pre")
y_dev = pickle_save.read_pickle("y_dev_pre")

max_len = 1169 # longest word sequence from the training set
n_chars = 51 # number of unique characters from training/dev sets
max_len_char = 54 # length of longest word from training/dev sets
n_pos_m = len(unique_pos)

#word input
input_word = Input(shape=(max_len,)) # shape will change according to the input data
words = Embedding(input_dim=np.size(embedding_matrix,0),  # these dimensions might also change
                  output_dim=np.size(embedding_matrix,1), 
                  weights=[embedding_matrix], # weights consist of the pre-loaded embedding matrix
                  trainable=True)(input_word)
words = Dropout(0.5)(words)

#POS input
input_pos = Input(shape=(max_len,))
pos = Embedding(input_dim=n_pos_m, 
                  output_dim=100, 
                  trainable=True)(input_pos)
pos = Dropout(0.5)(pos)

model = concatenate([words,pos])
model = Bidirectional(LSTM(units=100, return_sequences=True, recurrent_dropout=0.1))(model)
out = TimeDistributed(Dense(5, activation="softmax"))(model)  # softmax output layer
model = Model(inputs=[input_word,input_pos], outputs=[out])
model.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])

print(model.summary())

history = model.fit([x_train,p_train], 
                    np.array(y_train).reshape(len(y_train), max_len, 5), 
                    batch_size=64, epochs=15, 
                    validation_data=([x_dev,p_dev], 
                                      np.array(y_dev).reshape(len(y_dev), max_len, 5)),
                   )

model.save("neural_models/15epoch_64batch_00_emb_pos_pre_drop5")
pickle_save.write_pickle(history,"history_15epoch_64batch_00_emb_pos_pre_drop5")